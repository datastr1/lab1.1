import java.util.Arrays;

public class ArrayManipulation {
    public static void main(String[] args) {
        
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        for(int i=0; i<numbers.length;i++){
            System.out.print(numbers[i]+" ");

        }
        System.out.print("\n______________");

        for(String n:names){
            System.out.println();
            System.out.print(n+" ");
        }
        System.out.print("\n______________");

        for(int i=0; i<values.length; i++){
            System.out.println();
            values[0] = 0.65;
            values[1] = 1.25;
            values[2] = 2.55;
            values[3] = 3.85;
            System.out.print(values[i]);

        }
        System.out.print("\n______________");

        int sum =0;
        for(int i=0; i<numbers.length; i++){
            sum += numbers[i];
        }
        System.out.println();
        System.out.print("sum is "+sum);
        System.out.print("\n______________");

        double max = values[0];
        for(int i=0; i<values.length; i++){
            if(values[i]>max) max = values[i];
        }
        System.out.println();
        System.out.print("max is "+max);
        System.out.print("\n______________");

        System.out.println();
        String[] reversedNames = {"Alice", "Bob", "Charlie", "David"};
        for(int i = reversedNames.length - 1; i >= 0; i--){
            System.out.print(reversedNames[i]+" ");
        }
        System.out.print("\n______________");

        System.out.println();
        Arrays.sort(numbers);
        System.out.println("ascending numbers = "+Arrays.toString(numbers));
        System.out.print("\n______________");
    }

}
